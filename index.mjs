
import express from 'express';
import path from 'node:path';
import { fileURLToPath } from 'node:url';

import { routes } from './Utils/routes.mjs';
import { friendsRouter } from './routes/friends.router.mjs';
import { messagesRouter } from './routes/messages.router.mjs';

const PORT = 3000;

//setup __dirname since it is not available in ES6 modules
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

//setup app server instance
const app = express();

//custom logging middleware
app.use((req, res, next) => {
    const startDate = new Date();
    const start = Date.now();
    next();
    const end = Date.now();
    const executionTime = end - start;
    console.log(`[${startDate.toLocaleString()}] ` +
    `${req.method} - ${req.url} - took ${executionTime}ms`);
});

//server static files middleware
app.use('/site', express.static(
path.join(__dirname, 'public')
));

//json parse middleware
app.use(express.json());

//routes
app.get(routes.root, (req, res) => {
   res.send('Hello from server'); 
});

//mounting friends router
app.use(routes.friends, friendsRouter);
app.use(routes.messages, messagesRouter);

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`)
});