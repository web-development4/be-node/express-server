
import path from 'node:path';
import { fileURLToPath } from 'node:url';

export function getMessages(req, res) {

    const __filename = fileURLToPath(import.meta.url);
    const __dirname = path.dirname(__filename);

    const route = path.join(__dirname, '..', 'public', 'mountain.png');
    
    res.sendFile(route);
}