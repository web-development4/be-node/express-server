
import { httpStatus } from "../Utils/httpStatus.mjs";

import { friends } from "../data/friends.mjs";

export function getFriends(req, res) {
    res.status(httpStatus.ok);
    res.json(friends);
};

export function postFriend(req, res) {
    if (req?.body?.name ?? false)
    {
        const newFriend = {
            name: req.body.name,
            id: friends.length
        }
        friends.push(newFriend);
        res.json(newFriend);
    } else {
        res.status(httpStatus.badRequest);
        res.json({error: 'Missing friend name'});
    }
};

export function getFriendById(req, res) {
    const friendId = Number.parseInt(req.params.friendId);
    if (isNaN(friendId)) {
        res.status(httpStatus.badRequest);
        res.json({error: 'Bad request'});
    } else {
        const friend = friends[friendId] ?? null;
        if (friend !== null) {
            res.status(httpStatus.ok);
            res.json(friend);
        } else {
            res.status(httpStatus.notFound);
            res.json({error: 'Friend does not exist'});
        }
    }
};