
import express from "express";

import { getMessages } from "../controllers/messages.controller.mjs";

//create router instance
export const messagesRouter = express.Router();

//Adding logging middleware
messagesRouter.use((req, res, next) => {
    console.log(`Messages router - Incoming request from IP: ${req.ip}`);
    next();
})

//setup endpoints
messagesRouter.get("/", getMessages);
