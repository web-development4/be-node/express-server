
import express from "express";

import { getFriends, getFriendById, postFriend } from "../controllers/friends.controller.mjs";

//create router instance
export const friendsRouter = express.Router();

//Adding logging middleware
friendsRouter.use((req, res, next) => {
    console.log(`Friends router - Incoming request from IP: ${req.ip}`);
    next();
})

//setup endpoints
friendsRouter.get("/", getFriends);
friendsRouter.post("/", postFriend)
friendsRouter.get("/:friendId", getFriendById);