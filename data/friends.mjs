
import { Friend } from "../Models/friend.model.mjs";

export const friends = [
    new Friend(0, 'John'),
    new Friend(1, 'Adam'),
    new Friend(2, 'Kolt')
];